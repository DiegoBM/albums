import React, {Component} from 'react';
import {ScrollView} from 'react-native';
import axios from 'axios';

import AlbumDetail from './AlbumDetail';

class AlbumList extends Component {
    state = {
        albums: []
    };

    componentWillMount() {
        const url = 'http://rallycoding.herokuapp.com/api/music_albums';
        axios.get(url).then(response => this.setState({albums: response.data}));
    }

    render() {
        return (
            <ScrollView>
                {this.state.albums.map(album => (<AlbumDetail key={album.title} {...album} />))}
            </ScrollView>
        );
    }
}

export default AlbumList;