import React from 'react';
import {Text, Image, View, Linking, StyleSheet} from 'react-native';

import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const AlbumDetail = ({title, artist, thumbnail_image, image, url}) => (
    <Card>
        <CardSection>
            <Image source={{uri: thumbnail_image}} style={styles.thumbnailImage} />
            <View style={styles.headerContainer}>
                <Text style={styles.albumTitle}>{title}</Text>
                <Text>{artist}</Text>
            </View>
        </CardSection>
        <CardSection>
            <Image source={{uri: image}} style={styles.mainImage} />
        </CardSection>
        <CardSection>
            <Button onPress={() => Linking.openURL(url)}>Purchase Now</Button>
        </CardSection>
    </Card>
);

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    thumbnailImage: {width: 50, height: 50, marginLeft: 10, marginRight: 10},
    albumTitle: {fontSize: 18},
    mainImage: {width: null, flex: 1, height: 300}
});

export default AlbumDetail;